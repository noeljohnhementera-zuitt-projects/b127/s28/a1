/*Create a fetch request using the GET method that will 
retrieve all the to do list items from JSON Placeholder API.*/

fetch("https://jsonplaceholder.typicode.com/todos")
  .then(response => response.json())
  .then(allTodos => console.log(allTodos))

/* create an array using the map method to return 
just the title of every item and print the result in the console.*/

fetch("https://jsonplaceholder.typicode.com/todos")
  .then(response => response.json())
  .then(allTodos => {
  	let titleList = allTodos.map(allTitle =>{
  		return allTitle.title
  	})
  	console.log(titleList);
  })

 /* Create a fetch request using the GET method that will 
 retrieve a single to do list item from JSON Placeholder API.*/

fetch("https://jsonplaceholder.typicode.com/todos/9")
  .then(response => response.json())
  .then(singleTodo => console.log(singleTodo))

 /* Using the data retrieved, 
 print a message in the console that will provide the title and status of the to do list item*/

fetch("https://jsonplaceholder.typicode.com/todos/9")
  .then(response => response.json())
  .then(singleTodo => 
  	console.log(`The title of this to do list is ${singleTodo.title} and the status is currently ${singleTodo.completed}`));

  /* Create a fetch request using the POST method that will create a 
  to do list item using the JSON Placeholder API.*/

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userID: 9,
		title: "uvuvwevwevwe onyetenyevwe ugwemubwem ossas",
		completed: false
	})
})
.then(response => response.json())
.then(newTodo => console.log(newTodo))

/*Create a fetch request using the 
PUT method that will update a to do list item using the JSON Placeholder API.*/

fetch("https://jsonplaceholder.typicode.com/todos/9", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userID: 9,
		title: "Updated Version: uvuvwevwevwe onyetenyevwe ugwemubwem ossas",
		completed: true
	})
})
.then(response => response.json())
.then(updateTodo => console.log(updateTodo))

/*Update a to do list item by changing the data structure to contain the following properties:*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Study and Code in a daily manner",
		description: "To do list to improve coding skills",
		status: true,
		dateCompleted: "September 14, 2021",
		userID: 1
	})
})
.then(response => response.json())
.then(updateTodo => console.log(updateTodo))

/*Create a fetch request using the 
PATCH method that will update a to do list item using the JSON Placeholder API.*/

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: true
	})
})
.then(response => response.json())
.then(partialUpdateTodo => console.log(partialUpdateTodo))

/*Update a to do list item by changing the status to complete and add a date when the status was changed.*/

fetch("https://jsonplaceholder.typicode.com/todos/24", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userID: 2,
		title: "delectus aut autem",
		completed: true,
		dateOfChangeStatus: "September 14, 2021"
	})
})
.then(response => response.json())
.then(updateTodo => console.log(updateTodo))

/*Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.*/
fetch("https://jsonplaceholder.typicode.com/todos/199", {
	method: "DELETE"
})
.then(response => response.json())
.then(deleteTodo => console.log(deleteTodo))